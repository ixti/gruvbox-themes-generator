A tiny script to that generates theme files for some software I use using
[gruvbox][] pallete:

- [vivid][]
- [kitty][]

[gruvbox]: https://github.com/gruvbox-community/gruvbox
[vivid]: https://github.com/sharkdp/vivid
[kitty]: https://sw.kovidgoyal.net/kitty/

### Kitty

``` bash
for n in {0..15}; do
  echo "$(printf '%2d' "$n"): \e[38;5;${n}m FG \e[0m \e[48;5;${n}m BG \e[0m"
done
echo "DEMO SELECTION"
```

---

![kitty](assets/kitty.png)
